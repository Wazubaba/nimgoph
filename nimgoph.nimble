# Package
version = "1.0.0"
author = "Wazubaba"
description = "A simple and minimalistic gopher server implemented in nim"
license = "GPLv3"
bin = @["src/main.nim"]

# Deps
requires "nim >= 0.20.2"
