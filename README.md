# N I M G O P H

## Introduction
`nimgoph` is a very simple gopher server implemented in nim.



## Configuration
Take a look in `sample/nimgoph.conf` for an example.


## Compiling
All you need is nim and nimble.

Right now we use nake for the build. You can use nimble to install nake:
`nimble install nake`

And from there just run `nake` for a list of options and settings,
and `nake [task]` without the brackets to invoke a task. Ex:
`nake release` is most likely what you want to use.

