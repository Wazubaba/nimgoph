import nake
import strformat
import strutils
import os
import terminal

const
  PROG_NAME = "dgopher"
  VERSION = "0.0.1"
  PREFIX = "." # Change this to alter where the executable looks for shit.
  DEBUG_SUFFIX = "_debug"

when defined(linux):
  const cachedir = ".linuxcache"
elif defined(unix):
  const cachedir = ".unixcache"
else:
  const cachedir = ".windowscache"

const
  g_prefix_arg = "--define:PREFIX:" & PREFIX
  g_version_arg = "--define:VERSION:" & VERSION
  g_cache_arg = "--nimcache:" & cachedir

proc printOption(name, desc: string) =
  styledWriteLine(stdout, "\t", fgMagenta, name, fgWhite, " - ", desc)

proc printTask(name, desc: string) =
  styledWriteLine(stdout, "\t", fgCyan, name, fgWhite, " - ", desc)

task "default", "yes":
  styledWriteLine(stdout, &"{PROG_NAME} has the following options:")
  printOption("PREFIX", "Change where the program looks for root")
  echo ""
  styledWriteLine(stdout, &"{PROG_NAME} has the following tasks:")
  for task in tasks.keys:
    if task == "default": continue
    printTask(task, tasks[task].desc)



task "debug", &"Build {PROG_NAME} in debug mode":
  const
    bin_arg = "--out:" & PROG_NAME & DEBUG_SUFFIX
    cache_arg = g_cache_arg & "_d"

  direShell(nimExe, "c", "--define:debug", g_prefix_arg, "--debuginfo",
  cache_arg, bin_arg, g_version_arg, join_path("src", "main.nim"))

task "release", &"Build {PROG_NAME} in release mode":
  const
    bin_arg = "--out:" & PROG_NAME
    cache_arg = g_cache_arg & "_r"

  direShell(nimExe, "c", "--define:release", "--opt:size", "--define:danger", g_prefix_arg,
  cache_arg, bin_arg, g_version_arg, join_path("src", "main.nim"))

  let stripbin = findExe("strip")
  let upxbin = findExe("upx")
  if stripbin.len > 0:
    direShell(stripbin, "--strip-unneeded", PROG_NAME)

  if upxbin.len > 0:
    direShell(upxbin, "--best", PROG_NAME)

task "clean", "Clean build files":
  const debug_name = PROG_NAME & DEBUG_SUFFIX
  const release_name = PROG_NAME

  if debug_name.fileExists():
    removeFile(debug_name)
  if release_name.fileExists():
    removeFile(release_name)

  for name in [".linuxcache", ".unixcache", ".windowscache"]:
    let d = name & "_d"
    let r = name & "_r"
    if d.dirExists(): removeDir(d)
    if r.dirExists(): removeDir(r)

task "dist-clean", "Restore to a fresh working directory":
  run_task("clean")
  removeFile("nakefile")

