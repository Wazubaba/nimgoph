# TODO: Cross-platformify this..
const
  PREFIX* {.strdefine.}: string = "/"

  ETC* = PREFIX & "etc"
  VAR* = PREFIX & "var"
  HOME* = PREFIX & "home"
  OPT* = PREFIX & "opt"
  BIN* = PREFIX & "bin"
  LIB* = PREFIX & "lib"
  USR* = PREFIX & "usr"

