import parsecfg
import strutils
import streams
#import msgpack4nim

{.experimental: "implicitDeref".}

const
  DEFAULT_ROOT = "/var/gopherdocs"
  DEFAULT_WORKERS = 5
  DEFAULT_PORT = 1234
  DEFAULT_NOTFOUND = "NOTFOUND"

type
  Config* = object
    network*: tuple[
      workers: int,
      port: int
    ]
    resolve*: tuple[
      root: string,
      notfound: string
    ]
    security*: tuple[
      whitelist: seq[string],
      blacklist: seq[string]
    ]

  ConfigRef* = ref Config

proc `$`*(self: Config): string =
  result &= "Contents of config:"
  result &= "\n\tnetwork.workers: " & $self.network.workers
  result &= "\n\tnetwork.port: " & $self.network.port
  result &= "\n===================================="
  result &= "\n\tresolve.root: " & self.resolve.root
  result &= "\n\tresolve.notfound: " & self.resolve.notfound
  result &= "\n===================================="
  result &= "\n\tsecurity.whitelist: " & $self.security.whitelist
  result &= "\n\tsecurity.blacklist: " & $self.security.blacklist

proc generic_config*: ConfigRef =
  result = new Config
  result.resolve.root = DEFAULT_ROOT
  result.resolve.notfound = DEFAULT_NOTFOUND
  result.network.workers = DEFAULT_WORKERS
  result.network.port = DEFAULT_PORT


proc digit_test(val: string): bool {.inline.} =
  for ch in val:
    if not ch.isDigit: return false
  return true

proc get_or_default(val: string, default: int): int {.inline.} =
  if val.len > 0 and val.digit_test(): return val.parseInt()
  else: return default

proc get_or_default(val: string, default: string): string {.inline.} =
  if val.len > 0: return val
  else: return default

proc fetch_configuration*(path: string): ConfigRef =
  result = new Config
  var fstream = newFileStream(path, fmRead)
  if fstream == nil: echo("Cannot open config file: ", path)
  else:
    var
      parser: CfgParser
      currentSec: string
    open(parser, fstream, path)
    while true:
      var evt = parser.next()
      case evt.kind:
        of cfgEof: break
        of cfgSectionStart:
          currentSec = evt.section
        of cfgKeyValuePair:
          case currentSec.toLower():
            of "network":
              case evt.key:
                of "workers": result.network.workers = get_or_default(evt.value, DEFAULT_WORKERS)
                of "port": result.network.port = get_or_default(evt.value, DEFAULT_PORT)
            of "resolve":
              case evt.key:
                of "root": result.resolve.root = get_or_default(evt.value, DEFAULT_ROOT)
                of "not_found": result.resolve.notfound = get_or_default(evt.value, DEFAULT_NOTFOUND)
            of "security.whitelist":
              result.security.whitelist.add(evt.key)
            of "security.blacklist":
              result.security.blacklist.add(evt.key)
        of cfgError:
          echo("Configuration Error: ", evt.msg)
        else: break
#[
  IDK why, but for some reason msgpack4nim isn't properly unpacking things... :(
proc dump*(cfg: Config, path: string) =
  #var mstream = MsgStream.init()
  let fstream = newFileStream(path, fmWrite)
  if fstream == nil: echo "ERROR: Failed to create cache of running configuration"
  else:
    fstream.pack(cfg)
    echo "Packed data: ", stringify(pack(cfg))

proc inject*(path: string): ConfigRef =
  let fstream = newFileStream(path, fmRead)
  if fstream == nil: echo "ERROR: Failed to load cache of running configuration"
  else:
    var data: Config
    fstream.unpack(data)
    echo $data

]#
