import json
import os
import net
import nativesockets
import strutils

import configuration
import prefixpath



{.experimental: "implicitDeref".}

const CONFIG_NAME {.strdefine.}: string = "nimgoph.conf"

when not defined(debug):
  const CONFIG_PATHS = [
    joinPath(".", CONFIG_NAME), # Check cwd
    joinPath(ETC, CONFIG_NAME)] # Check etc
else:
  # Only check project sample dir
  const CONFIG_PATHS = [joinPath("sample", CONFIG_NAME)]

var
  config: ConfigRef

# Find configuration file

for path in CONFIG_PATHS:
  if fileExists(path):
    echo "Info: Loading config from '", path, "'"
    config = fetch_configuration(path)
    break

if config == nil:
  stderr.writeLine("Failed to load configuration. Falling back to default values")
  config = generic_config()

# Configuration is now loaded. Handle overriding via command-line args
# TODO: Implement commandline args handling

proc sanitize_newlines(content: var string) =
  content = multiReplace(content, ("\n", "\r\n"))

proc do_lookup_resolution(address, req: string): string =
  ## Tries to return the correct path to the requested document or NOTFOUND instead.
  ## If neither are found and valid files it screams at you
  echo "Request from ", address, " for ", if req == "\r\n": "/" else: req
  var path: string
  if req in ["\r\n", "/"]: path = config.resolve.root / "gophermap"
  else:
    path = config.resolve.root / req
    if req.contains(ParDir):
      # Someone is doing something fucky...
      echo "!WARNING! ATTEMPT TO ACCESS OUT-OF-ROOT PATH: ", path
      echo "!WARNING! Address is '", address, "'"
      path = ""

  if path.existsFile():
    echo "[OK] Resource found at ", path
    return path
  else:
    echo "[ERR] Resource not found, attempting to return ", config.resolve.notfound
    let errpath = config.resolve.root / config.resolve.notfound
    if not errpath.existsFile():
      echo "[CRITICAL] Server is not configured correctly. Unable to locate ", config.resolve.notfound, ". READ THE DOOOOCCCCSSSS! REEEEEEEEEAAAAAAAAAAAAAD THEM OR THIS PURPOSFULLY OVERLY LONG MESSAGE WILL KEEEEEEPP SSSCCCRRRREEEEAAAAAMMMIIIINNNNGGGGG AAAAATTTTT YYYYYOOOOOOOOUUUUUU AAAAAAUUUUUUUUUUUUUGGGGGGGGGGGGGGGGHHHHHHHHHHH"
      return ""
    else:
      return errpath

proc clientHandler(address: string, client: Socket) =
  ## Get requested resource and deliver it or an error
  echo "Connection from ", address
  var req = client.recvLine()
  let path = do_lookup_resolution(address, req)

  if path == "":
    client.send("ERROR. Resource is not found and my owner messed up their configuration too! :(")
  else:
    echo "[LOAD] ", path
    var data = path.readFile()
    data.sanitize_newlines()
    client.send(data)


# This is exactly the wrong way to do this. We need to create a thing that just assigns the next available thread to
# a newly incoming socket. When that thread completes, we report via msgbus that we are all done. Unless there is a
# better way... Perhaps via testing thread.running() ? Idk...
#for i in 0..workers.high:
#  createThread(workers[i], clientHandler, (conn: nil, req: "taco", msgBus: msgBus.addr))

var serverSock = newSocket()
serverSock.setSockOpt(OptReuseAddr, true)
serverSock.bindAddr(Port(config.network.port))
serverSock.listen()

var
  client: Socket
  address: string
  #backlog: seq[tuple[client: Socket, address: string]]

while true:
  serverSock.acceptAddr(client, address)
  var dns: string
  echo "DEBUG: [", address, "]"
  if address.isIpAddress():
    let dns_lookup = address.getHostByAddr()
    echo "Resolved IP [", address, "] to [", dns_lookup.name, "]"
    if "localhost" in dns_lookup.aliases:
      echo "Is localhost, assuming dns address [localhost] for security.(white|black)list tests"
      dns = "localhost"
    else: dns = dns_lookup.name
  else:
    dns = address

  if config.security.whitelist.len > 0 and dns notin config.security.whitelist:
    echo "WARN: Connection from non-whitelisted address '", dns, "'"
  elif config.security.blacklist.len > 0 and dns in config.security.blacklist:
    echo "WARN: Connection from blacklisted address '", dns, "'"
  else:
    client_handler(address, client)
  client.close()


echo "Info: Using root '", config.resolve.root, "'"
#echo "Info: Initializing ", config.network.workers, " threads"

echo "Info: Whitelisting ", config.security.whitelist
echo "Info: Blacklisting ", config.security.blacklist

